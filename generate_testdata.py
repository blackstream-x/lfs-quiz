#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
generate_testdata.py

Generate testdata for the LFS quiz
"""

import itertools
import pathlib
import secrets
import subprocess
import tarfile
import tempfile

from typing import Iterator, List, Set


IM_BACKGROUND = "white"
IM_FILL = "black"
IM_CONVERT = "convert"
IM_FONT = "Liberation-Sans"
IM_SIZE = "200x200"

IMAGE_FILE_TYPES = ("png", "gif")
TEXT_FILE_TYPES = ("gig", "gnf", "gng", "oif", "pig", "pnf")

UTF8 = "utf-8"


def create_image_file(
    file_path: pathlib.Path,
    im_label: str,
    im_background: str = IM_BACKGROUND,
    im_fill: str = IM_FILL,
    im_font: str = IM_FONT,
    im_size: str = IM_SIZE,
):
    """Create an image file"""
    subprocess.run(
        (
            "convert",
            "-background",
            im_background,
            "-fill",
            im_fill,
            "-size",
            im_size,
            "-font",
            im_font,
            f"label:{im_label}",
            str(file_path),
        ),
        check=True,
    )


def create_text_file(
    file_path: pathlib.Path,
    contents: str,
):
    """Create a text file"""
    file_path.write_text(contents, encoding=UTF8)


def permutate_case(extension: str) -> Iterator[str]:
    """Yield case permutations for the given extension"""
    combinations: List[str] = []
    for char in extension:
        lower = char.lower()
        upper = char.upper()
        if lower == upper:
            combinations.append(char)
        else:
            combinations.append(f"{lower}{upper}")
        #
    #
    seen_values: set[str] = set()
    for value in itertools.product(*combinations):
        if value in seen_values:
            continue
        #
        seen_values.add(value)
        yield "".join(value)
    #


def create_all_files():
    """Create all testdata files"""
    with tempfile.TemporaryDirectory() as tempdir:
        binary_files: List[str] = []
        text_files: List[str] = []
        dest_dir = pathlib.Path(tempdir) / "testdata"
        dest_dir.mkdir(parents=True)
        stub = secrets.token_hex(8)
        seen_stubs: Set[str] = set([stub])
        # Add image files
        for extension in IMAGE_FILE_TYPES:
            for ext in permutate_case(extension):
                while stub in seen_stubs:
                    stub = secrets.token_hex(8)
                #
                seen_stubs.add(stub)
                file_name = f"{stub}.{ext}"
                binary_files.append(file_name)
                target_path = dest_dir / file_name
                create_image_file(target_path, stub[:2])
            #
        #
        # Add text files
        for extension in TEXT_FILE_TYPES:
            while stub in seen_stubs:
                stub = secrets.token_hex(8)
            #
            seen_stubs.add(stub)
            file_name = f"{stub}.{extension}"
            text_files.append(file_name)
            target_path = dest_dir / file_name
            create_text_file(target_path, stub)
        #
        # Add check files
        lfs_check_file = "lfs-handled.txt"
        git_check_file = "git-handled.txt"
        create_text_file(
            dest_dir / lfs_check_file,
            "\n".join(binary_files),
        )
        text_files.extend((lfs_check_file, git_check_file, ".gitattributes"))
        create_text_file(
            dest_dir / git_check_file,
            "\n".join(text_files),
        )
        #
        # Tar destination directory
        # and write the zip file to cwd
        with tarfile.TarFile(
            pathlib.Path.cwd() / "testdata.tar",
            mode="w",
        ) as tar_file:
            tar_file.add(dest_dir, arcname="testdata")
        #
    #


if __name__ == "__main__":
    create_all_files()


# vim:fileencoding=utf-8 sw=4 ts=4 sts=4 expandtab autoindent syntax=python
