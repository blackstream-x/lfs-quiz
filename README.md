# Git LFS Quiz

## Quiz

How to track all files with Git LFS having the same extension,
but in a case insensitive manner?

---
> **Motivation**
>
> While working on a repository conversion project,
> I made a mistake that led to a situation where
> not all desired files were tracked with LFS
> after switching execution from Windows to Linux.
>
> The cause was obvious: case sensitive filesystems in
> Unix/Linux versus case-insensitive on Windows.
>
> I decided to "gamify" the test of my proposed solution
> – here it is, for Unix/Linux.

---


## Rules:

1. all binary files **must** be handled by LFS
2. all text files **must** be handled by Git itself
3. lfs tracking rules **must** be based on file extensions only
4. try to use as few lines in .gitattributes as possible
   to achieve higher scores


## Requirements

- Git
- Git LFS (package name is usually `git-lfs`)
- bc
- Python 3 (if you want to run the testdata generation script yourself)
- Standard Unix tools including `awk`, `bash`, `diff`, `sort`, `tar` and `wc`

---
> :warning: **Initialize Git LFS**
>
> Make sure Git LFS is not only installed as a package
> **but also initialized** using `git lfs install`
> **before cloning this repository**,
> otherwise you will neither be able to extract the tarball
> nor to solve the quiz.

---


## Test data

Use the `testdata.tar` file directly
or generate testdata yourself using the Python script
[generate_testdata.py](./generate_testdata.py).

The test data tar file contains GIF and PNG files
with file extensions in different case mixes,
and text files with various (non-standard) extensions.


## Extract test data to a new directory and initialize a new Git repository

Change to the parent directory of this repository, then:

```
tar xf lfs-quiz/testdata.tar
cd testdata
git init
```


## Track all PNG and GIF files using LFS

Track all image files (PNG and GIF formats) using Git LFS,
but use as few commands as possible.

Avoid tracking any text files
(in this case: all files that are neither GIF nor PNG files,
determined by the extension).

Command(s): `git lfs track …`


## Evaluation

Use the [evaluate.sh](./evaluate.sh) script
to evaluate your results.

Points on success:

| Number of lines in .gitattributes | Points | Remarks |
|:--------------------------------- | ------:|:------- |
| 1                                 | 800    | theoretical maximum value |
| 2                                 | 100    | expected result with basic Unix knowledge |
| 3                                 | 29     |         |
| 4                                 | 12     |         |
| 5                                 | 6      |         |
| 6                                 | 3      |         |
| 7                                 | 2      |         |
| 8                                 | 1      |         |
| 9                                 | 1      |         |
| 10 or more                        | 0      |         |

As for the stochastic parrots, pardon, »AI«:
I guess they are not able yet
to complete this quiz with a decent score.
But feel free to try it out!

This statement might not age well – I am quite sure that
at some point in time somebody will expicitly
»teach« the »AI« how to solve this
if this humble quiz should get significant attention.
