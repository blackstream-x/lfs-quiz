#!/bin/bash

# Verify LFS quiz results

if [ ! -f .gitattributes ] ; then
    echo "FAILED – no .gitattributes file (ie. no LFS handled files)"
    echo "Use the command »git lfs track ...« to handle files with LFS."
    git reset
    exit 1
fi

git add .
git_lfs_status=$(git lfs status)
git_actual=$(echo "${git_lfs_status}" | awk '$2 == "(Git:" { print $1 }' | sort)
git_files_diff=$(echo "${git_actual}" | diff - <(sort git-handled.txt))
# diff output must be empty!
if [ "empty${git_files_diff}" != "empty" ] ; then
    echo "FAILED – Difference in Git handled files:"
    echo "${git_files_diff}"
    echo "Try again."
    git reset
    exit 1
fi

lfs_actual=$(echo "${git_lfs_status}" | awk '$2 == "(LFS:" { print $1 }' | sort)
lfs_files_diff=$(echo "${lfs_actual}" | diff - <(sort lfs-handled.txt))
# diff output must be empty!
if [ "empty${lfs_files_diff}" != "empty" ] ; then
    echo "FAILED – Difference in LFS handled files:"
    echo "${lfs_files_diff}"
    echo "Try again."
    git reset
    exit 1
fi

gitattributes_lines=$(wc -l < .gitattributes)
points=$(echo "800 / ${gitattributes_lines}^3" | bc)
echo "SUCCEEDED – reached points: ${points}"
